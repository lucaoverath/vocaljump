﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Android;
using UnityEngine.Audio;
using System.IO;
using System;

public class EventHandler : MonoBehaviour
{
    public List<GameObject> Panels;
    public Dropdown dd;
    public static string activeMic;

    private void Start()
    {

        foreach (GameObject g in Panels)
        {
            if (g.name == "MainMenu")
            {
                g.SetActive(true);
            }
            else
            {
                g.SetActive(false);
            }
        }
        List<string> mics = new List<string>();
        foreach (string mic in Microphone.devices)
        {
            mics.Add(mic);
        }
        activeMic = mics[0];
        dd.AddOptions(mics);
    }

    private void Update()
    {
        activeMic = dd.options[dd.value].text;
    }

    public void LoadMyScene(int scene)
    {
        Debug.Log("trying to change scene.");
        SceneManager.LoadScene(scene);
    }
    


    public void Exit()
    {
        Application.Quit();
    }
}
