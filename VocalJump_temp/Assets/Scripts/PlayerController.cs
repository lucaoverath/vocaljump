﻿using System;
using System.Threading;
using System.Collections;
using System.IO.Ports;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


[RequireComponent(typeof(AudioSource))]
public class PlayerController : MonoBehaviour
{
    //game stuff
    private Rigidbody2D rb;
    public float movementSpeed = 1, Jumpforce = 1;
    private bool grounded = false;
    private bool levitate = false;
    public float upwardsForceMultiplier = 2;
    private Transform pos;
    public GameObject levitateBar;
    public GameObject deathMenu;

    //microphone stuff
    private string[] microphones = { "", "", "", "", "", "", "", "", "", "" };
    private string microphone;
    int minSamplingFreq = 0, maxSamplingFreq = 0;
    const int MICRONUMBER = 0;
    const int LOWESTFREQUENCY = 60;
    public float MINVOLUMEPEAK = 0.1f;
    private float[] sampleArray;
    private int sampleArrayLength;
    private float maxValue;
    AudioSource src;
    readonly int _sampleWindow = 128;

    //arduino
    public SerialPort sp = new SerialPort("COM3", 9600, Parity.None, 8, StopBits.One); // assigning port
    private bool blnPortCanOpen = false; //become's true when port is open
    static private int databyte_out; //index in txChars array of possible characters to send
    static private bool databyteWrite = false; //to let the serial com thread know there is a byte to send

    //txChars contains the characters to send: we have to use the index
    private char[] txChars = { '0', '1' , '2', '3', '4'};

    //threadrelated
    private bool stopSerialThread = false; //to stop the thread
    private Thread readWriteSerialThread; //threadvariabele



    void Start()
    {
        OpenConnection();
        readWriteSerialThread = new Thread(SerialThread);
        readWriteSerialThread.Start();

        rb = GetComponent<Rigidbody2D>();

        //Adding all microphones in a list
        int index = 0;
        foreach (string device in Microphone.devices)
        {
            //Debug.Log(device);
            microphones[index] = device;
            index++;
            if (index == 10)
            {
                Debug.Log("Max. 1O input devices...");
                break;
            }
        }

        //choose microphone
        microphone = microphones[MICRONUMBER];
        Microphone.GetDeviceCaps(microphone, out minSamplingFreq, out maxSamplingFreq);
        Debug.Log("Frequency: " + microphone + " = " + maxSamplingFreq);
        //maxFreq is maximum sampling frequency: lengthOfLineRenderer and sampleArray have maxFreq/MINFREQ*2 samples 48000/60*2 = 1600 samples
        sampleArrayLength = maxSamplingFreq / LOWESTFREQUENCY * 2;
        sampleArray = new float[sampleArrayLength];
        Debug.Log("number of samples = " + sampleArrayLength);
        src = GetComponent<AudioSource>();
        //remove any soundfile in the audiosource
        src.clip = null;
        //start recording from microphone in loop, 1 second is minimum, use maxFreq from selected microphone
        src.clip = Microphone.Start(EventHandler.activeMic, true, 999, maxSamplingFreq);

    }

    void Update()
    {
        //src.timeSamples = 0;
        src.clip.GetData(sampleArray, maxSamplingFreq / 2);//put the samples from the half of the recording of 1 second in the array (microphone stabilised)
        //search max value in the sampleArray
        maxValue = MaxValueSampleArray();

        UpdateLEDs(maxValue);
    }

    void FixedUpdate()
    {
        rb.AddForce(Vector2.right * movementSpeed);

        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            rb.AddForce(Vector2.up * Jumpforce);
        }

        if (maxValue > MINVOLUMEPEAK && grounded) //check if volume is acceptable 
        {
            rb.AddForce(Vector2.up * Jumpforce);
        }

        if (maxValue > MINVOLUMEPEAK && levitate)
        {
            Levitate();
        }
    }

    private void UpdateLEDs(float maxValue)
    {
        if (maxValue > .5)
        {
            Debug.Log(maxValue);
        }

        if (maxValue < MINVOLUMEPEAK)
        {
            databyte_out = 0; //index in txChars
            databyteWrite = true;
            Debug.Log("0 is pressed");
        }
        if (maxValue > MINVOLUMEPEAK && maxValue < (MINVOLUMEPEAK + 0.05f))
        {
            databyte_out = 1; //index in txChars
            databyteWrite = true;
            Debug.Log("1 is pressed");
        }
        if (maxValue > (MINVOLUMEPEAK + 0.05f) && maxValue < (MINVOLUMEPEAK + 0.1f))
        {
            databyte_out = 2; //index in txChars
            databyteWrite = true;
            Debug.Log("2 is pressed");
        }
        if (maxValue > (MINVOLUMEPEAK + 0.1f) && maxValue < (MINVOLUMEPEAK + 0.15f))
        {
            databyte_out = 3; //index in txChars
            databyteWrite = true;
            Debug.Log("3 is pressed");
        }
        if (maxValue > (MINVOLUMEPEAK + 0.2f))
        {
            databyte_out = 4; //index in txChars
            databyteWrite = true;
            Debug.Log("4 is pressed");
        }
    }

    private void Levitate()
    {
        Vector2 upwardsForce = Vector2.zero;
        if (rb.velocity.y < 0)
        {
            upwardsForce -=  new Vector2(0, rb.velocity.y * upwardsForceMultiplier);
        }
        rb.AddForce(upwardsForce);
    }

    float MaxValueSampleArray()
    {
        float maxValue = 0.0f;
        float[] waveData = new float[_sampleWindow];
        int micPosition = Microphone.GetPosition(null) - (_sampleWindow + 1);
        if (micPosition < 0) return 0;
        src.clip.GetData(waveData, micPosition);
        for (int i = 0; i < _sampleWindow; i++)
        {
            float wavePeak = waveData[i] * waveData[i];
            if (wavePeak > maxValue)
            {
                maxValue = wavePeak;
            }
        }
        return maxValue;
    }

    public void OnCollisionEnter2D(Collision2D collider)
    {

        if (collider.gameObject.tag == "Ground")
        {
            grounded = true;
        }

        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Levitate")
        {
            levitate = true;
            SpriteRenderer color= collision.gameObject.GetComponent<SpriteRenderer>();
            if (maxValue > MINVOLUMEPEAK)
            {
                color.color = Color.green;
            }
            else
            {
                color.color = Color.red;
            }
        }
    }
    

    public void OnCollisionStay2D(Collision2D collider)
    {
        if (collider.gameObject.tag == "Ground")
        {
            grounded = true;
        }
        
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Levitate")
        {
            Debug.Log("In red box");
            levitate = true;
            SpriteRenderer color = collision.gameObject.GetComponent<SpriteRenderer>();

            if (maxValue > MINVOLUMEPEAK)
            {
                color.color = Color.green;
            }
            else
            {
                color.color = Color.red;
            }
        }
        
    }


    public void OnCollisionExit2D(Collision2D collider)
    {
        if (collider.gameObject.tag == "Ground")
        {
            grounded = false;
        }

        

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Levitate")
        {
            levitate = false;
            SpriteRenderer color = collision.gameObject.GetComponent<SpriteRenderer>();
            
                color.color = Color.red;
        }
        if (collision.gameObject.name == "deathbarier")
        {
            deathMenu.SetActive(true);
            Time.timeScale = 0.0f;
        }
    }

    private void OnApplicationQuit()
    {
        src.Stop();
        src.clip = null;
        Microphone.End(microphone);
        if (sp != null) sp.Close();
        stopSerialThread = true;
        readWriteSerialThread.Abort();
    }



    //Function connecting to Arduino
    public void OpenConnection()
    {
        if (sp != null)
        {
            if (sp.IsOpen)
            {
                string message = "Port is already open!";
                Debug.Log(message);
            }
            else
            {
                try
                {
                    sp.Open();  // opens the connection
                    blnPortCanOpen = true;
                }
                catch (Exception e)
                {
                    Debug.Log(e.Message);
                    blnPortCanOpen = false;
                }
                if (blnPortCanOpen)
                {
                    sp.ReadTimeout = 20;  // sets the timeout value before reporting error
                    Debug.Log("Port Opened!");
                }
            }
        }
        else
        {
            Debug.Log("Port == null");
        }
    }

    void SerialThread() //separate thread is needed because we need to wait sp.ReadTimeout = 20 ms to see if a byte is received
    {
        while (!stopSerialThread) //close thread on exit program
        {
            if (blnPortCanOpen)
            {
                if (databyteWrite)
                {
                    sp.Write(txChars, databyte_out, 1); //tx 'A' or tx 'U'
                    databyteWrite = false; //to be able to send again
                }
            }
        }
    }

    
}
