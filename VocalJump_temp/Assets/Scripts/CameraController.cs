﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public float smoothness = 0.1f;
    void Update()
    {
        if (target)
        {
            Vector3 desiredPos = new Vector3(target.position.x + 15, target.position.y, target.position.z - 10);
            transform.position = Vector3.Lerp(transform.position, desiredPos, smoothness);
        }
    }
}
