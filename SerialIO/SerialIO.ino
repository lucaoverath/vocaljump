#define DRUKKNOP 8
#define GREEN 2
#define YELLOW1 3
#define YELLOW2 4
#define RED 5

char serialInputChar;
void setup()
{
  Serial.begin(9600);
  pinMode(DRUKKNOP,INPUT_PULLUP);
  digitalWrite(GREEN,LOW);
  digitalWrite(YELLOW1,LOW);
  digitalWrite(YELLOW2,LOW);
  digitalWrite(RED,LOW);
  pinMode(GREEN,OUTPUT);
  pinMode(YELLOW1,OUTPUT);
  pinMode(YELLOW2,OUTPUT);
  pinMode(RED,OUTPUT);
  
}

void loop()
{
  if (Serial.available())
  {
    serialInputChar = Serial.read();
    if (serialInputChar == '0')
    {
      digitalWrite(GREEN, LOW);
      digitalWrite(YELLOW1,LOW);
      digitalWrite(YELLOW2,LOW);
      digitalWrite(RED,LOW);
    }
    if (serialInputChar == '1')
    {
      digitalWrite(GREEN, HIGH);
      digitalWrite(YELLOW1,LOW);
      digitalWrite(YELLOW2,LOW);
      digitalWrite(RED,LOW);
    }
    if (serialInputChar == '2')
    {
      digitalWrite(GREEN, HIGH);
      digitalWrite(YELLOW1,HIGH);
      digitalWrite(YELLOW2,LOW);
      digitalWrite(RED,LOW);
    }
    if (serialInputChar == '3')
    {
      digitalWrite(GREEN, HIGH);
      digitalWrite(YELLOW1,HIGH);
      digitalWrite(YELLOW2,HIGH);
      digitalWrite(RED,LOW);
    }
    if (serialInputChar == '4')
    {
      digitalWrite(GREEN, HIGH);
      digitalWrite(YELLOW1,HIGH);
      digitalWrite(YELLOW2,HIGH);
      digitalWrite(RED,HIGH);
    }
  }
  
  if (digitalRead(DRUKKNOP) == LOW)
  {
    Serial.write('T');
    delay(50); //debounce
    while (digitalRead(DRUKKNOP) == LOW);
    delay(50); //debounce
  }
}
